from selenium.webdriver.support.ui import Select
from time import sleep


class Addcustomer:
    text_cx_link_xpath = "//p[normalize-space()='Customers']//i[contains(@class,'right fas fa-angle-left')]"
    text_cx_lnk = "//a[@href='/Admin/Customer/List']//p[contains(text(),'Customers')]"
    Add_new_btn = "(//i[@class='fas fa-plus-square'])[1]"
    email_button_text = "//input[@id='Email']"
    password_txt = "//input[@id='Password']"
    Firstname_text = "//input[@id='FirstName']"
    Last_name = "//input[@id='LastName']"
    Gender_radio_btn = "//input[@id='Gender_Male']"
    Gender_radio_btn1 = "//input[@id='Gender_Female']"
    dob = "//input[@id='DateOfBirth']"
    mgr_of_vendors = "//select[@id='VendorId']"
    company_name = "//input[@id='Company']"
    Tax_checkbox = "//input[@id='IsTaxExempt']"
    Newsletter = "//div[@class='k-widget k-multiselect k-multiselect-clearable k-state-hover k-state-border-down']//div[@role='listbox']"
    roles = "//div[@class='input-group-append input-group-required']//input[@role='listbox']"
    selected_registered = "//li[normalize-space()='Registered']"
    Selected_admin = "//li[normalize-space()='Administrators']"
    Selected_guest = "//li[normalize-space()='Guests']"
    Selected_vendors = "//li[normalize-space()='Vendors']"
    Active_checkbox = "//input[@id='Active']"
    Admin_comment = "//textarea[@id='AdminComment']"
    Save = "//button[@name='save']//i[@class='far fa-save']"
    Save_and_continue = "://button[normalize-space()='Save and Continue Edit']"

    def __init__(self, driver):
        self.driver = driver

    def click_on_customers_menu(self):
        self.driver.find_element("xpath", self.text_cx_link_xpath).click()

    def click_on_click_on_customers_menuitem(self):
        self.driver.find_element("xpath", self.text_cx_lnk).click()

    def click_on_add_new(self):
        self.driver.find_element("xpath", self.Add_new_btn).click()

    def enter_email(self, email):
        self.driver.find_element("xpath", self.email_button_text).send_keys(email)

    def enter_password(self, password):
        self.driver.find_element("xpath", self.password_txt).send_keys(password)
        sleep(4)

    def customer_roles(self, role):
        self.driver.find_element("xpath", self.roles).click()
        sleep(4)
        if role == "Registered":
            self.list = self.driver.find_element("xpath", self.selected_registered)
        elif role == "Administrators":
            self.list = self.driver.find_elment("xpath", self.Selected_admin)
        elif role == "Guests":
            sleep(3)
            self.driver.find_element("xpath", "//span[@title='delete']").click()
            self.list = self.driver.find_element("xpath", self.Selected_guest)
        elif role == "Registered":
            self.list = self.driver.find_element("xpath", self.selected_registered)
        elif role == "Vendors":
            self.list = self.driver.find_element("xpath", self.Selected_vendors)
        else:
            self.list = self.driver.find_element("xpath", self.Selected_guest)
        sleep(4)
        self.driver.execute_script("arguments[0].click();", self.list)

    def set_managers_of_vendors(self, value):
        drp = Select(self.driver.find_element("xpath", self.mgr_of_vendors))
        drp.select_by_index(value)

    def set_gender(self, gender):
        if gender == "Male":
            self.driver.find_element("xpath", self.Gender_radio_btn).click()
        elif gender == "Female":
            self.driver.find_element("xapth", self.Gender_radio_btn1).click()
        else:
            self.driver.find_element("xpath", self.Gender_radio_btn).click()

    def enter_first_name(self, fname):
        self.driver.find_element("xpath", self.Firstname_text).send_keys(fname)

    def enter_last_name(self, lname):
        self.driver.find_element("xpath", self.Last_name).send_keys(lname)

    def enter_dob(self, dob):
        self.driver.find_element("xpath", self.dob).send_keys(dob)

    def set_cname(self, cname):
        self.driver.find_element("xpath", self.company_name).send_keys(cname)

    def click_on_save(self):
        self.driver.find_element("xpath", self.Save).click()
