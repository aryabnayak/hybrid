
class Loginpage:
    username = "Username"
    password = "Password"
    login = "(//input[@class='btn blue-button'])[2]"
    prof = "//span[@class='user-actions-ico']//*[name()='svg']"
    logout="//span[text()='Log out']"


    def __init__(self,driver):
        self.driver = driver

    def usr(self,Username):
        self.driver.find_element("id", "Email").clear()
        self.driver.find_element("id", "Email").send_keys(Username)
    def pwd(self,Password):
        self.driver.find_element("id","Password").clear()
        self.driver.find_element("id","Password").send_keys(Password)
    def logg(self):
        self.driver.find_element("xpath","//span[normalize-space()='Log in']").click()
    def lgin(self):
        self.driver.find_element("xpath","//button[normalize-space()='Log in']").click()
    def prf(self):
        self.driver.find_element("xpath","//span[@class='user-actions-ico']//*[name()='svg']").click()
    def lgout(self):
        self.driver.find_element("xpath","//span[text()='Log out']").click()
