from selenium import webdriver
import pytest


@pytest.fixture()
def setup(browser):
    if browser=="Chrome":
        driver = webdriver.Chrome("./chromedriver.exe")
    elif browser=="firefox":
        driver = webdriver.Firefox()
    else:
        driver = webdriver.Chrome()
    return driver
# this will get the value from cli
def pytest_addoption(parser):
    parser.addoption("--browser")
# this will return thhe browser value to setupo
@pytest.fixture()
def browser(request):
    return request.config.getoption("--browser")

# ********html report************
# it is a hook for adding the enviornment info in html
def pytest_configure(config):
    config._metadata['project name'] = 'nop commerce'
    config._metadata['module'] = 'customers'
    config._metadata['Testers'] = 'arya'
# it is a hook for delete/modify the enviornment info in html
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop('Java_Home','None')
    metadata.pop('Plugins','None')
