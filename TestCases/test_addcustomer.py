import pytest
from time import sleep
from PageObjects.Login import Loginpage
from PageObjects.Addcustomepage import Addcustomer
from Utilities.read import ReadConfig
from Utilities.customlogger import Loggen
from selenium.webdriver.common.by import By
import string
import random


class Test_add_customer:
    URL = ReadConfig.getappurl()
    Username = ReadConfig.getusername()
    Password = ReadConfig.getpass()
    logger = Loggen.loggen()

    @pytest.mark.Sanity
    def test_addcustromer(self,setup):
        self.logger.info("********Test Started")
        self.driver = setup
        self.driver.get(self.URL)
        self.driver.maximize_window()

        self.log = Loginpage(self.driver)
        self.log.usr(self.Username)
        self.log.pwd(self.Password)
        self.log.lgin()
        sleep(4)
        self.logger.info("******Login successful")

        self.logger.info("*****actual test started")

        self.add_cust = Addcustomer(self.driver)
        self.add_cust.click_on_customers_menu()
        sleep(4)
        self.add_cust.click_on_click_on_customers_menuitem()

        self.add_cust.click_on_add_new()

        self.logger.info("******providing information*******")
        # self.email = random_generator() + "gmail.com"
        self.add_cust.enter_email("aryabnayak1234@gmail.com")
        self.add_cust.enter_password("Test123")
        self.add_cust.enter_first_name("Arya")
        self.add_cust.enter_last_name("Nayak")
        self.add_cust.set_gender("Male")
        self.add_cust.enter_dob("12/17/1993")
        self.add_cust.set_cname("testyantra")
        self.add_cust.customer_roles("Guests")
        sleep(4)
        self.add_cust.set_managers_of_vendors(0)
        self.add_cust.click_on_save()

        self.logger.info("******saving information******")

        self.logger.info("******validation started******")
        self.msg = self.driver.find_element(By.TAG_NAME,"body").text

        print(self.msg)

        if "The new customer has been added successfully." in self.msg:
            assert True == True
        else:
            self.driver.save_screenshot(".\\screenshots\\"+"add customer.png" )
            self.logger.error("*****not added******")
            assert True == False




def random_generator(size = 8, chars = string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))
