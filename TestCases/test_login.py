import pytest

from PageObjects.Login import Loginpage

from Utilities.read import ReadConfig
from Utilities.customlogger import Loggen
class Testlogin:
    base_url = ReadConfig.getappurl()
    username = ReadConfig.getusername()
    password = ReadConfig.getpass()

    logger  = Loggen.loggen()

    @pytest.mark.Sanity
    def test_title(self,setup):
        self.logger.info("*******Test case1")
        self.logger.info("*********verifying**********")
        self.driver=setup
        self.driver.get(self.base_url)
        title = self.driver.title
        if title=="Login - nopCommerce":
            assert True
            self.driver.close()
            self.logger.info("*********passed*****")
        else:
            self.driver.save_screenshot(".\\Screenshots\\"+"hometitle.png")
            self.driver.close()
            self.logger.error("********failed******")
            assert False

    @pytest.mark.Regression
    def test_log(self,setup):
        self.driver = setup
        self.driver.get(self.base_url)
        self.log = Loginpage(self.driver)
        self.log.usr(self.username)
        self.log.pwd(self.password)
        self.log.lgin()
        title = self.driver.title
        self.driver.close()
        if title=="Store Demo - nopCommerce":
            assert True
        else:
            assert  False
