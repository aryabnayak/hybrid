from PageObjects.Login import Loginpage

from Utilities.read import ReadConfig
from Utilities.customlogger import Loggen
from Utilities import XLUtils
from time import sleep

class Testlogin_DDT:
    base_url = ReadConfig.getappurl()
    TestData = ".//Test_Data/Book1.xlsx"
    logger = Loggen.loggen()

    def test_log(self, setup):
        self.driver = setup
        self.driver.get(self.base_url)
        self.log = Loginpage(self.driver)
        self.rows = XLUtils.getRowcount(self.TestData, 'Sheet1')
        print("no of rows in excel is", self.rows)

        status = []
        for r in range(2, self.rows + 1):
            self.username = XLUtils.readData(self.TestData, 'Sheet1', r, 1)
            self.password = XLUtils.readData(self.TestData, 'Sheet1', r, 2)
            self.Result = XLUtils.readData(self.TestData, 'Sheet1', r, 3)

            self.log.usr(self.username)
            self.log.pwd(self.password)
            self.log.lgin()
            title = self.driver.title
            sleep(5)

            exp_titple = "Store Demo - nopCommerce"
            if title == exp_titple:
                if self.Result == 'pass':
                    self.logger.info("****passed*****")
                    self.log.prf()
                    self.log.lgout()
                    sleep(5)
                    self.log.prf()
                    sleep(4)
                    self.log.logg()
                    status.append("pass")
                elif self.Result == 'Fail':
                    self.logger.info("*****Failed***")
                    self.log.prf()
                    self.log.lgout()
                    self.log.prf()
                    sleep(5)
                    self.log.logg()
                    sleep(3)
                    status.append("fail")
            elif title != exp_titple:
                if self.Result == 'passed':
                    self.logger.info("***failed***")
                    status.append("fail")
                elif self.Result == "Failed":
                    self.logger.info("***passed***")
                    status.append("pass")

        if "fail" not in status:
            self.logger.info("test passed")
            self.driver.close()
            assert True

        else:
            self.logger.info("test failed")
            self.driver.close()
            assert False
