import logging

class Loggen:
    @staticmethod
    def loggen():
        logging.basicConfig(filename=".\\Log\\automation.log",
                        format='%(asctime)s: %(levelname)s : %(message)s',datefmt="%m/%d/%y %I:%M:%S %P")
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        return logger

