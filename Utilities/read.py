import configparser

config = configparser.RawConfigParser()
config.read(".\\congiguration\\Config.ini")

class ReadConfig:
    @staticmethod
    def getappurl():
        url=config.get("Datas","base_url")
        return url

    @staticmethod
    def getusername():
        user=config.get("Datas","username")
        return user

    @staticmethod
    def getpass():
        passw = config.get("Datas", "password")
        return passw
